package datasources;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

public class DatabaseHandlerTest {

	@Test
	public void test() throws SQLException {
		DatabaseHandler db = new DatabaseHandler("scott", "tiger");
		assertNotNull(db.query("SELECT table_name FROM user_tables"));
	}

}
