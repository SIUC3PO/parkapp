package datasources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DatabaseHandler is a class for connecting to the database and getting results
 * in an easy to parse format. Queries are returned as array lists and
 * inserts/deletes give feedback to the call all while keeping the database
 * information private like it should be.
 *
 */
public class DatabaseHandler {

	private static final Logger LOGGER = Logger.getLogger( DatabaseHandler.class.getName() );
	private String server="jdbc:oracle:thin:@131.230.133.11:1521:cs";
	private String username = "ebyrne";
	private String password = "DpIvAEbf";
	private Connection connection;
	
	public DatabaseHandler() throws SQLException {
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		connection = DriverManager.getConnection(server, username, password);
		LOGGER.log( Level.INFO, "Connected to the database" );
	}
	
	public DatabaseHandler(String usrname, String passwrd) throws SQLException{
		// Create a new connection to the database with the Oracle Drivers
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		connection = DriverManager.getConnection(server, usrname, passwrd);
		LOGGER.log( Level.INFO, "Connected to the database" );
	}
	
	/**
	 * Query opens a statement process with the database and passes the query in
	 * returning a list of arrays as the result closing the process as well to avoid
	 * wasted resources.
	 * 
	 * @param query A SQL statement with SELECT FROM WHERE properly written without the semicolon.
	 * @return
	 * @throws SQLException
	 */
	public List<String[]> query(String query) throws SQLException {
		// Open the query
		PreparedStatement statement = connection.prepareStatement(query);
		ResultSet results = statement.executeQuery();
		ResultSetMetaData queryMeta = results.getMetaData();
		
		// Get column count and create a container for the results
		int columnCount = queryMeta.getColumnCount();
		List<String[]> table = new ArrayList<>();
		
		// add results
		while( results.next()) {
			String[] row = new String[columnCount];
		    for( int column = 1; column <= columnCount; column++ ){
		            Object obj = results.getObject( column );
		            row[column - 1] = (obj == null) ?null:obj.toString();
		    } table.add( row );
		}
		
		statement.close();; // Since we saved results, close query to free resources
		LOGGER.log( Level.INFO, "Returning results" );
		return table; // Finally return the results in list form
	}
	
	/**
	 * Performs a SQL statement of insert, delete, or update on the open database returning true or 
	 * false depending on the outcome
	 * 
	 * @param query A SQL statement for inserting, deleting or updating a tuple without the semicolon.
	 * @throws SQLException
	 */
	public void execute(String query) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(query);
		if( !statement.execute() ) {
			LOGGER.log( Level.INFO, "Execution was successful" );
		} else {
			LOGGER.log( Level.INFO, "Execution failed" );
		}
		statement.close();
	}
	public void closeConnection() throws SQLException { connection.close(); }
}
