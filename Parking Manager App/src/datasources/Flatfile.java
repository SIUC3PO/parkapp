package datasources;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import utilities.StringCSV;
import datasources.DataEnums;

public class Flatfile implements IDatabase {
	private HashMap<String, Integer> schema;
	private List<String> table;
	private String dbfile;
	
	public Flatfile() {
		super();
		this.schema = new HashMap<>();
		this.table = Collections.emptyList();
	}

	@Override
    public void loadSchema() {
		// TODO Auto-generated method stub
		
		if(table.isEmpty()) { return; }
		
		String[] tokens = table.get(0).split(",");
		int i=0;
		for(String token: tokens) {
			this.schema.put(token, i);
			i++;
		}
	}

	@Override
	public int selectTable(String table) {
		if(!this.table.isEmpty()) { return 1;}
		this.dbfile = table;
	    try {
	    	this.table = Files.readAllLines(Paths.get(this.dbfile), StandardCharsets.UTF_8);
	    } catch (IOException e) { 
	    	return DataEnums.SELECT_TABLE_FAIL.Value;
	    }
		
		return 0;
	}

	@Override
	public int insertEntry(String[] entry) {
		Path path = Paths.get("./", this.dbfile);
		
	    byte[] bytes = StringCSV.toCSV(entry).getBytes();

		try {
		    Files.write(path, bytes, StandardOpenOption.APPEND);
		} catch (IOException e) {
			return DataEnums.INSERT_FAIL.Value;
		}
		
		return DataEnums.SUCCESS.Value;
	}

	@Override
	public int deleteEntry(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateEntry(int id, String field, String value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String[] selectEntry(String where) {
		// TODO Auto-generated method stub
		return null;
	}

}
