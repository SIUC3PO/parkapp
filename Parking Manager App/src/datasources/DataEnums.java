package datasources;

import java.util.HashMap;
import java.util.Map;

public enum DataEnums {
	SUCCESS(0), CONNECTION_FAIL(1), SELECT_TABLE_FAIL(2), INSERT_FAIL(3), DELETE_FAIL(4), UPDATE_FAIL(5), SELECT_FAIL(6);
	
	public final int Value;
	
	private DataEnums(int value)
	{
		Value = value;
	}
	
	private static final Map<Integer, DataEnums> _map = new HashMap<Integer, DataEnums>();
	static
	{
	    for (DataEnums databaseenums : DataEnums.values())
	        _map.put(databaseenums.Value, databaseenums);
	}
	
	public static DataEnums from(int value)
	{
	    return _map.get(value);
	}
}
