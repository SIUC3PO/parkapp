package datasources;

public interface IDatabase {
	public void loadSchema();
	public int selectTable(String from);
	public int insertEntry(String[] entry);
	public int deleteEntry(int id);
	public int updateEntry(int id, String field, String value);
	public String[] selectEntry(String where);
}
