package app;

import java.util.HashMap;
import java.util.Map;

public enum AppEnums {
	LOGIN_SUCCESFUL(0), LOGIN_INVALID(1), LOGIN_ATTEMPTS_MAX(2), ADD_DATA_FAILURE(3), REMOVE_DATA_FAILURE(4), MODIFY_DATA_FAILURE(5);
	
	public final int Value;
	
	private AppEnums(int value)
	{
		Value = value;
	}
	
	private static final Map<Integer, AppEnums> _map = new HashMap<Integer, AppEnums>();
	static
	{
	    for (AppEnums databaseenums : AppEnums.values())
	        _map.put(databaseenums.Value, databaseenums);
	}
	
	public static AppEnums from(int value)
	{
	    return _map.get(value);
	}
}
