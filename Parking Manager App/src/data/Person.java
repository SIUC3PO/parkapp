package data;

public class Person {
	protected int id;
	protected String username;
	protected String password;
	protected String fullname;
	protected String email;
	protected String phone;
	protected Boolean _active;
	protected Boolean _session_expired;
	
	public Person(int id, String username, String password, String fullname, String email, String phone) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this._active = true;
		this._session_expired = true;
	}
	
	public Person(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public static int checkLogin(String username, String password) {
		
		// if the login is successful return 0 else return some error enum or not zero
		int valid = 0;
		
		return valid;
	}
	
	public static int createAccount() {
		//A one time means of the user being able to set?
		return 0;
	}
	
	
}
