package utilities;

public class StringCSV {
	public static String toCSV(String[] stringIn){
		String csvOut = String.join(",", stringIn);
		return csvOut;
	}
}
